# Natwest Sender  #

Sender service responsible to post the encrypted transaction details to natwest-receiver service. There a POST API exposed name createTransaction to accept the transaction details and encrypt.

## Heroku deployment link ##
https://subash-natwest-sender.herokuapp.com/

## API Details ##

1. Create Transaction
   * URL - https://subash-natwest-sender.herokuapp.com/v1/transaction
   * Sample Request Body
   ```
   {
      "accountNumber": "3434343",
      "amount": 69200.34,
      "type": "Debit",
      "currency": "INR",
      "accountFrom": "6555223213"
   }
   ```
   * Sample Response
   ```
   {
      "code": "NS-50001",
      "description": "Record created successful"
   }
   ```

## Swagger File and Postman collection ##

* [Swagger File](https://bitbucket.org/natwest-hackathon/natwest-sender/src/master/src/main/resources/api.yml)
* [Postman Collection](https://bitbucket.org/natwest-hackathon/natwest-properties-repo/src/master/FakeQueue%20Heroku.postman_collection.json)

