FROM adoptopenjdk/openjdk16:alpine-jre
ADD target/natwest-sender-0.0.1-SNAPSHOT.jar natwest-sender.jar
ENTRYPOINT ["java","-jar","natwest-sender.jar"]
