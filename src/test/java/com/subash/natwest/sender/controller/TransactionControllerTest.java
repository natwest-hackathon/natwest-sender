package com.subash.natwest.sender.controller;


import com.subash.natwest.sender.model.Transaction;
import com.subash.natwest.sender.service.SenderService;
import com.subash.natwest.sender.utilsForTest.TestUtils;
import com.subash.natwest.sender.validator.TransactionValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {TransactionController.class})
public class TransactionControllerTest {

    @Autowired
    TransactionController transactionController;

    @MockBean
    SenderService service;

    @MockBean
    TransactionValidator transactionValidator;

    @Test
    public void createTransaction() {
        try {
            Transaction transaction = TestUtils.getTransaction();
            // given
            transactionController.createTransaction(transaction);
            //verify
            verify(service, times(1)).createTransaction(anyString(), eq(transaction));
        } catch (Exception e) {
            fail();
            e.printStackTrace();
        }
    }
}
