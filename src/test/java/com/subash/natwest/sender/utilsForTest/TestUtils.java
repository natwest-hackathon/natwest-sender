package com.subash.natwest.sender.utilsForTest;

import com.subash.natwest.sender.model.Transaction;

public class TestUtils {

    public static Transaction getTransaction() {
        Transaction transaction = new Transaction();
        transaction.setAccountFrom("34342434367664");
        transaction.setAccountNumber("5667656");
        transaction.setAmount(55000d);
        transaction.setCurrency("INR");
        transaction.setType("Credit");
        return transaction;
    }
}
