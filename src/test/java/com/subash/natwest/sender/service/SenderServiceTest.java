package com.subash.natwest.sender.service;

import com.subash.natwest.sender.dto.EncryptedTransaction;
import com.subash.natwest.sender.httpclient.SaveTransactionClient;
import com.subash.natwest.sender.model.GenericResponse;
import com.subash.natwest.sender.model.Transaction;
import com.subash.natwest.sender.util.Constants;
import com.subash.natwest.sender.utilsForTest.TestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@SpringBootTest(classes = {SenderService.class})
public class SenderServiceTest {

    @MockBean
    SaveTransactionClient saveTransactionClient;

    @Autowired
    SenderService service;

    @Test
    public void createTransaction() {
        try {
            Transaction transaction = TestUtils.getTransaction();
            GenericResponse response = new GenericResponse();
            response.setCode(Constants.DECRYPT_SUCCESS_CODE);
            response.setDescription(Constants.DECRYPT_SUCCESS_DESC);
            given(saveTransactionClient.decryptAndSaveTransaction(any(EncryptedTransaction.class))).willReturn(response);
            GenericResponse actualResponse = service.createTransaction("454545-04454545", transaction).getBody();
            assertEquals(Constants.CREATE_SUCCESS_CODE, actualResponse.getCode());
        } catch (Exception e) {
            fail();
            e.printStackTrace();
        }
    }
}
