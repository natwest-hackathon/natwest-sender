package com.subash.natwest.sender.httpclient;

import com.subash.natwest.sender.dto.EncryptedTransaction;
import com.subash.natwest.sender.model.GenericResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "NATWEST-RECEIVER", url = "${feign.client.url.savetransaction}")
public interface SaveTransactionClient {

    @PostMapping("/savetransaction")
    public GenericResponse decryptAndSaveTransaction(@RequestBody EncryptedTransaction encryptedTransaction);


}
