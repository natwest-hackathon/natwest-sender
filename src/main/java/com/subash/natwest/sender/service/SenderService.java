package com.subash.natwest.sender.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.subash.natwest.sender.dto.EncryptedTransaction;
import com.subash.natwest.sender.httpclient.SaveTransactionClient;
import com.subash.natwest.sender.model.GenericResponse;
import com.subash.natwest.sender.model.Transaction;
import com.subash.natwest.sender.util.AESUtil;
import com.subash.natwest.sender.util.GenericLogger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import static com.subash.natwest.sender.util.Constants.*;

@Component
public class SenderService {

    private static final Logger logger = LogManager.getLogger(SenderService.class);

    @Autowired
    SaveTransactionClient saveTransactionClient;

    /**
     * Method helps to encrypt the transaction details
     *
     * @param UUID
     * @param transaction
     * @return
     */
    public ResponseEntity<GenericResponse> createTransaction(String UUID, Transaction transaction) {
        GenericResponse response = new GenericResponse();
        try {
            // Encrypt logic

            ObjectMapper mapper = new ObjectMapper();
            String transactionString = mapper.writeValueAsString(transaction);
            String transactionEncrypted = AESUtil.encrypt(transactionString, SECRET_KEY);
            logger.info("Encrypted Transaction : " + transactionEncrypted);
            EncryptedTransaction encryptedTransaction = new EncryptedTransaction();
            encryptedTransaction.setTransactionString(transactionEncrypted);

            // FeignClient API call
            response = saveTransactionClient.decryptAndSaveTransaction(encryptedTransaction);

            if (response != null && DECRYPT_SUCCESS_CODE.equals(response.getCode())) {
                response.code(CREATE_SUCCESS_CODE);
                response.description(CREATE_SUCCESS_DESC);
                GenericLogger.logResponse(logger, UUID, "SUCCESS", response);
                return ResponseEntity.ok(response);
            } else {
                response.code(CREATE_ERROR_CODE);
                response.description(CREATE_ERROR_CODE);
                GenericLogger.logResponse(logger, UUID, "ERROR", response);
                return ResponseEntity.internalServerError().build();
            }

        } catch (Exception e) {
            GenericLogger.logError(logger, UUID, "ERROR", e);
            // Set and log response
            response.code(CREATE_ERROR_CODE);
            response.description(CREATE_ERROR_CODE);
            GenericLogger.logResponse(logger, UUID, "ERROR", response);
            return ResponseEntity.internalServerError().build();
        }
    }
}
