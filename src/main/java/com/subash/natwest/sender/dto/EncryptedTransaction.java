package com.subash.natwest.sender.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * EncryptedTransaction
 */
@Setter
@Getter
public class EncryptedTransaction {

    private String transactionString;
}

