package com.subash.natwest.sender.validator;


import com.subash.natwest.sender.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Locale;

@Component
public class TransactionValidator implements Validator {


    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> clazz) {
        return Transaction.class.equals(clazz);
    }

    @Override
    public void validate(Object transaction, Errors errors) {

        //accountNumber
        ValidationUtils.rejectIfEmpty(errors, "accountNumber",
                messageSource.getMessage("natwest.sender.transaction.accountNumber.error", null, Locale.getDefault()));

        //amount
        ValidationUtils.rejectIfEmpty(errors, "amount",
                messageSource.getMessage("natwest.sender.transaction.amount.error", null, Locale.getDefault()));

        //type
        ValidationUtils.rejectIfEmpty(errors, "type",
                messageSource.getMessage("natwest.sender.transaction.type.error", null, Locale.getDefault()));

        //currency
        ValidationUtils.rejectIfEmpty(errors, "currency",
                messageSource.getMessage("natwest.sender.transaction.currency.error", null, Locale.getDefault()));

        //accountFrom
        ValidationUtils.rejectIfEmpty(errors, "accountFrom",
                messageSource.getMessage("natwest.sender.transaction.accountFrom.error", null, Locale.getDefault()));

    }
}
