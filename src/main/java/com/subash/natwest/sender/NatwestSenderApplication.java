package com.subash.natwest.sender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class NatwestSenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(NatwestSenderApplication.class, args);
	}

}
