package com.subash.natwest.sender.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sender")
public class VersionController {

    @GetMapping("/version")
    public String getVersion() {
        return "0.1";
    }
}
