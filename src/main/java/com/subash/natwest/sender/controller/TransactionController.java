package com.subash.natwest.sender.controller;

import com.subash.natwest.sender.model.GenericResponse;
import com.subash.natwest.sender.model.Transaction;
import com.subash.natwest.sender.service.SenderService;
import com.subash.natwest.sender.util.Constants;
import com.subash.natwest.sender.util.GenericLogger;
import com.subash.natwest.sender.validator.TransactionValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("v1")
public class TransactionController implements TransactionApi {

    private static final Logger logger = LogManager.getLogger(TransactionController.class);

    @Autowired
    private TransactionValidator transactionValidator;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(transactionValidator);
    }

    @Autowired
    private SenderService service;

    @Override
    public ResponseEntity<GenericResponse> createTransaction(@Valid Transaction transaction) {
        String uuid = GenericLogger.getUUID();
        GenericLogger.logRequest(logger, uuid, Constants.CREATE_TRANSACTION, "POST", transaction);
        return service.createTransaction(uuid, transaction);
    }
}
